use ruma::{
    api::client::{membership::join_room_by_id, message::send_message_event},
    events::room::message::RoomMessageEventContent,
    RoomId, TransactionId,
};

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<(), Error> {
    let config = Config::load_from_path(std::path::Path::new("config.toml"))?;

    let (Some(room_id), Some(msg)) = (std::env::args().nth(1), std::env::args().nth(2)) else {
        return Err(Error(format!(
            "Usage: {} <room_id> <msg>",
            std::env::args().next().unwrap()
        )));
    };
    send_msg(
        config.homeserver_url,
        &config.username,
        &config.password,
        &room_id,
        &msg,
    )
    .await?;
    Ok(())
}

#[derive(Debug, Clone, serde::Deserialize)]
struct Config {
    pub homeserver_url: String,
    pub username: String,
    pub password: String,
}

impl Config {
    fn load_from_path(path: &std::path::Path) -> Result<Config, Error> {
        let content = std::fs::read_to_string(path)?;
        Ok(toml::from_str(&content)
            .map_err(|err| format!("Syntax error in file {}: {}", path.display(), err))?)
    }
}

async fn send_msg(
    homeserver_url: String,
    username: &str,
    password: &str,
    room_id: &str,
    msg: &str,
) -> Result<(), Error> {
    let client = ruma::Client::builder()
        .homeserver_url(homeserver_url)
        .http_client(ruma::client::http_client::Reqwest::new())
        .await?;
    client
        .log_in(username, password, None, Some("send-matrix"))
        .await?;
    let room_id = RoomId::parse(room_id)?;
    client
        .send_request(join_room_by_id::v3::Request::new(room_id.clone()))
        .await?;
    client
        .send_request(send_message_event::v3::Request::new(
            room_id,
            TransactionId::new(),
            &RoomMessageEventContent::text_plain(msg),
        )?)
        .await?;

    Ok(())
}

struct Error(String);

impl<D: std::fmt::Display> From<D> for Error {
    fn from(e: D) -> Self {
        Error(format!("{}", e))
    }
}
impl std::fmt::Debug for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.pad(self.0.as_str())
    }
}
