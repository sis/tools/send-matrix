# send-matrix

A minimal matrix client which sends messages to a specific room.

It can be built as musl binary and fits into a 4MB docker container.

## Compile

Requirements: rust

```sh
cargo build --release
```

## Usage

Create a file `config.toml`:

```toml
username = "@chucknorris:matrix.org"
password = "Chuck Norris does not need a password, the password needs him."
homeserver_url = "https://matrix.org"
```

Then run from the directory containing `config.toml`:

```sh
send-matrix <room_id> <msg>
```

You can obtain `<room_id>` from e.g. element by clicking on the
"Room settings" dropdown menu -> "Settings" -> "Advanced" -> "Internal room ID".
